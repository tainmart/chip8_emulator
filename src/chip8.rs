use crate::instruction::*;

use rand::Rng;
use std::fs;

const MEMORY_SIZE: usize = 4096;
const NUM_STACKS_FRAMES: usize = 16;
const NUM_CPU_REGISTER: usize = 16;
pub const NUM_KEYS: usize = 16;
pub const DISPLAY_WIDTH: usize = 64;
pub const DISPLAY_HEIGHT: usize = 32;
const FONT_WIDTH: u16 = 5;

pub struct Chip8 {
    opcode: u16,
    memory: [u8; MEMORY_SIZE],
    stack: [u16; NUM_STACKS_FRAMES],
    cpu_register: [u8; NUM_CPU_REGISTER],
    index_register: u16,
    program_counter: u16,
    delay_timer: u8,
    pub sound_timer: u8,
    stack_pointer: u8,
    pub keys: [bool; NUM_KEYS],
    pub display: [bool; DISPLAY_WIDTH * DISPLAY_HEIGHT],
}

impl Chip8 {
    pub fn new() -> Self {
        println!("Initializing Chip8..");
        let mut chip8 = Chip8 {
            opcode: 0,
            memory: [0; MEMORY_SIZE],
            stack: [0; NUM_STACKS_FRAMES],
            cpu_register: [0; NUM_CPU_REGISTER],
            index_register: 0,
            program_counter: 0x200,
            delay_timer: 0,
            sound_timer: 0,
            stack_pointer: 0,
            keys: [false; NUM_KEYS],
            display: [false; DISPLAY_WIDTH * DISPLAY_HEIGHT],
        };

        let font_set: [u8; 80] = [
            0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
            0x20, 0x60, 0x20, 0x20, 0x70, // 1
            0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
            0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
            0x90, 0x90, 0xF0, 0x10, 0x10, // 4
            0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
            0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
            0xF0, 0x10, 0x20, 0x40, 0x40, // 7
            0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
            0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
            0xF0, 0x90, 0xF0, 0x90, 0x90, // A
            0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
            0xF0, 0x80, 0x80, 0x80, 0xF0, // C
            0xE0, 0x90, 0x90, 0x90, 0xE0, // D
            0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
            0xF0, 0x80, 0xF0, 0x80, 0x80, // F
        ];
        for (index, &byte) in font_set.iter().enumerate() {
            chip8.memory[index] = byte;
        }

        println!("Chip8 initialized");
        chip8
    }

    pub fn load_game(&mut self, game_name: &String) {
        println!("Loading game: {}", game_name);
        let contents = fs::read(game_name);

        for (index, instruction) in contents.unwrap().iter().enumerate() {
            self.memory[(self.program_counter as usize) + index] = *instruction;
        }
    }

    pub fn emulate_cycle(&mut self) {
        if self.delay_timer > 0 {
            self.delay_timer -= 1;
        }

        if self.sound_timer > 0 {
            self.sound_timer -= 1;
        }

        self.opcode = (self.memory[self.program_counter as usize] as u16) << 8
            | self.memory[(self.program_counter as usize) + 1] as u16;

        self.program_counter = self.handle_opcode();
    }

    fn handle_opcode(&mut self) -> u16 {
        let hex_instruction = HexInstruction::new(self.opcode);
        let instruction = hex_instruction
            .to_instruction()
            .expect("Invalid Instruction");

        match instruction {
            Instruction::ClearDisplay() => {
                self.display = [false; DISPLAY_WIDTH * DISPLAY_HEIGHT];
                self.program_counter + 2
            }
            Instruction::ReturnFromSubRoutine() => {
                self.stack_pointer -= 1;
                let address = self.stack[(self.stack_pointer) as usize];
                address + 2
            }
            Instruction::JumpToAddress(address) => address,
            Instruction::ExecuteSubRoutine(address) => {
                self.stack[self.stack_pointer as usize] = self.program_counter;
                self.stack_pointer += 1;
                address
            }
            Instruction::SkipIfEqual(register, value) => {
                if self.read_register(register) == value {
                    self.program_counter += 2;
                }
                self.program_counter + 2
            }
            Instruction::SkipIfNotEqual(register, value) => {
                if self.read_register(register) != value {
                    self.program_counter += 2;
                }
                self.program_counter + 2
            }
            Instruction::SkipIfReqisterEqual(register1, register2) => {
                if self.read_register(register1) == self.read_register(register2) {
                    self.program_counter += 2;
                }
                self.program_counter + 2
            }
            Instruction::StoreValue(register, value) => {
                self.write_register(register, value);
                self.program_counter + 2
            }
            Instruction::AddValue(register, value) => {
                let current_register_value = self.read_register(register);
                self.write_register(register, current_register_value.wrapping_add(value));
                self.program_counter + 2
            }
            Instruction::CopyRegister(register1, register2) => {
                let register2_value = self.read_register(register2);
                self.write_register(register1, register2_value);
                self.program_counter + 2
            }
            Instruction::OrRegister(register1, register2) => {
                let new_value = self.read_register(register1) | self.read_register(register2);
                self.write_register(register1, new_value);
                self.program_counter + 2
            }
            Instruction::AndRegister(register1, register2) => {
                let new_value = self.read_register(register1) & self.read_register(register2);
                self.write_register(register1, new_value);
                self.program_counter + 2
            }
            Instruction::XorRegister(register1, register2) => {
                let new_value = self.read_register(register1) ^ self.read_register(register2);
                self.write_register(register1, new_value);
                self.program_counter + 2
            }
            Instruction::AddRegister(register1, register2) => {
                let (new_value, carry) = self
                    .read_register(register1)
                    .overflowing_add(self.read_register(register2));
                self.write_register(0xF, carry as u8);
                self.write_register(register1, new_value);
                self.program_counter + 2
            }
            Instruction::SubRegister(register1, register2) => {
                let (new_value, borrow) = self
                    .read_register(register1)
                    .overflowing_sub(self.read_register(register2));
                self.write_register(0xF, (borrow as u8) ^ 1);
                self.write_register(register1, new_value);
                self.program_counter + 2
            }
            Instruction::ShiftRightRegister(register) => {
                let value = self.read_register(register);
                self.write_register(0xF, value & 0x1);
                self.write_register(register, value >> 1);
                self.program_counter + 2
            }
            Instruction::SwapSubRegister(register1, register2) => {
                let (new_value, borrow) = self
                    .read_register(register2)
                    .overflowing_sub(self.read_register(register1));
                self.write_register(0xF, (borrow as u8) ^ 1);
                self.write_register(register1, new_value);
                self.program_counter + 2
            }
            Instruction::ShiftLeftRegister(register) => {
                let value = self.read_register(register);
                self.write_register(0xF, (value & 0x80) >> 7);
                self.write_register(register, value << 1);
                self.program_counter + 2
            }
            Instruction::SkipIfReqisterNotEqual(register1, register2) => {
                if self.read_register(register1) != self.read_register(register2) {
                    self.program_counter += 2;
                }
                self.program_counter + 2
            }
            Instruction::StoreIndex(address) => {
                self.index_register = address;
                self.program_counter + 2
            }
            Instruction::JumpPlusZero(address) => {
                address.wrapping_add(self.read_register(0x0) as u16)
            }
            Instruction::Random(register, value) => {
                let mut rng = rand::thread_rng();
                let random_value = rng.gen_range(u8::min_value(), u8::max_value());
                self.write_register(register, random_value & value);
                self.program_counter + 2
            }
            Instruction::Draw(register1, register2, value) => {
                let starting_x = self.read_register(register1) as usize;
                let starting_y = self.read_register(register2) as usize;

                let sprite_from = self.index_register as usize;
                let sprite_to = sprite_from + (value as usize);
                let sprite_data = &self.memory[sprite_from..sprite_to];

                let mut pixel_unset = false;

                for (byte, sprite_pixel) in sprite_data.iter().enumerate() {
                    let y = (starting_y + byte) % DISPLAY_HEIGHT;
                    for bit in 0..8 {
                        let x = (starting_x + bit) % DISPLAY_WIDTH;

                        let display_position = x + y * DISPLAY_WIDTH;

                        let current_pixel = self.display[display_position] as u8;
                        let bit_to_check = (sprite_pixel >> (7 - bit)) & 1;
                        let new_pixel = current_pixel ^ bit_to_check;
                        if !pixel_unset && new_pixel == 0 && current_pixel == 1 {
                            pixel_unset = true;
                        }
                        self.display[display_position] = new_pixel != 0;
                    }
                }

                self.write_register(0xF, pixel_unset as u8);
                self.program_counter + 2
            }
            Instruction::SkipIfPressed(register) => {
                if self.keys[self.read_register(register) as usize] {
                    self.program_counter += 2
                }
                self.program_counter + 2
            }
            Instruction::SkipIfNotPressed(register) => {
                if !self.keys[self.read_register(register) as usize] {
                    self.program_counter += 2
                }
                self.program_counter + 2
            }
            Instruction::LoadDelayTimer(register) => {
                self.write_register(register, self.delay_timer);
                self.program_counter + 2
            }
            Instruction::WaitForKeyPress(register) => {
                if self.keys == [false; NUM_KEYS] {
                    return self.program_counter;
                }

                for (index, is_pressed) in self.keys.iter().enumerate() {
                    if *is_pressed {
                        self.write_register(register, index as u8);
                        break;
                    }
                }

                self.program_counter + 2
            }
            Instruction::SetDelayTimer(register) => {
                self.delay_timer = self.read_register(register);
                self.program_counter + 2
            }
            Instruction::SetSoundTimer(register) => {
                self.sound_timer = self.read_register(register);
                self.program_counter + 2
            }
            Instruction::AddToIndex(register) => {
                self.index_register += self.read_register(register) as u16;
                self.program_counter + 2
            }
            Instruction::LoadSprite(register) => {
                let font_value = self.read_register(register) as u16;
                self.index_register = font_value * FONT_WIDTH;

                self.program_counter + 2
            }
            Instruction::StoreBcdRepresenation(register) => {
                let value = self.read_register(register);
                self.memory[self.index_register as usize] = value / 100;
                self.memory[(self.index_register + 1) as usize] = (value / 10) % 10;
                self.memory[(self.index_register + 2) as usize] = (value % 100) % 10;
                self.program_counter + 2
            }
            Instruction::StoreRegisters(register) => {
                for current_register in 0..register + 1 {
                    let index = (self.index_register + current_register as u16) as usize;
                    self.memory[index] = self.read_register(current_register);
                }
                self.program_counter + 2
            }
            Instruction::LoadRegisters(register) => {
                for current_register in 0..register + 1 {
                    let index = (self.index_register + current_register as u16) as usize;
                    self.write_register(current_register, self.memory[index]);
                }
                self.program_counter + 2
            }
        }
    }

    fn read_register(&mut self, register: Register) -> Value {
        self.cpu_register[register as usize]
    }

    fn write_register(&mut self, register: Register, value: Value) {
        self.cpu_register[register as usize] = value;
    }
}
