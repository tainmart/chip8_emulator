pub type Address = u16; // NNN
pub type Register = u8; // X,Y
pub type Value = u8; // KK

#[derive(Debug)]
pub enum Instruction {
    ClearDisplay(),                             // 00E0
    ReturnFromSubRoutine(),                     // 00EE
    JumpToAddress(Address),                     // 1NNN
    ExecuteSubRoutine(Address),                 // 2NNN
    SkipIfEqual(Register, Value),               // 3XKK
    SkipIfNotEqual(Register, Value),            // 4XKK
    SkipIfReqisterEqual(Register, Register),    // 5XY0
    StoreValue(Register, Value),                // 6XKK
    AddValue(Register, Value),                  // 7XKK
    CopyRegister(Register, Register),           // 8XY0
    OrRegister(Register, Register),             // 8XY1
    AndRegister(Register, Register),            // 8XY2
    XorRegister(Register, Register),            // 8XY3
    AddRegister(Register, Register),            // 8XY4
    SubRegister(Register, Register),            // 8XY5
    ShiftRightRegister(Register),               // 8XY6
    SwapSubRegister(Register, Register),        // 8XY7
    ShiftLeftRegister(Register),                // 8XYE
    SkipIfReqisterNotEqual(Register, Register), // 9XY0
    StoreIndex(Address),                        // ANNN
    JumpPlusZero(Address),                      // BNNN
    Random(Register, Value),                    // CXKK
    Draw(Register, Register, Value),            // DXYN
    SkipIfPressed(Register),                    // EX9E
    SkipIfNotPressed(Register),                 // EXA1
    LoadDelayTimer(Register),                   // FX07
    WaitForKeyPress(Register),                  // FX0A
    SetDelayTimer(Register),                    // FX15
    SetSoundTimer(Register),                    // FX18
    AddToIndex(Register),                       // FX1E
    LoadSprite(Register),                       // FX29
    StoreBcdRepresenation(Register),            // FX33
    StoreRegisters(Register),                   // FX55
    LoadRegisters(Register),                    // FX65
}

#[derive(Debug)]
struct AddressParts {
    most_significant_bit: u16,
    least_significant_bit: u16,
    x_register: Register,
    y_register: Register,
    value: Value,
    address: Address,
}

impl AddressParts {
    pub fn new(op_code: u16) -> Self {
        AddressParts {
            most_significant_bit: (op_code >> 12) & 0xF,
            least_significant_bit: op_code & 0xF,
            x_register: ((op_code >> 8) & 0xF) as Register,
            y_register: ((op_code >> 4) & 0xF) as Register,
            value: (op_code & 0xFF) as Value,
            address: op_code & 0xFFF,
        }
    }
}

#[derive(Debug)]
pub struct HexInstruction {
    address_parts: AddressParts,
}

impl HexInstruction {
    pub fn new(value: u16) -> Self {
        HexInstruction {
            address_parts: AddressParts::new(value),
        }
    }

    pub fn to_instruction(&self) -> Option<Instruction> {
        let parts = &self.address_parts;

        match parts.most_significant_bit {
            0x0 => match parts.value {
                0xE0 => Some(Instruction::ClearDisplay()),
                0xEE => Some(Instruction::ReturnFromSubRoutine()),
                _ => None,
            },
            0x1 => Some(Instruction::JumpToAddress(parts.address)),
            0x2 => Some(Instruction::ExecuteSubRoutine(parts.address)),
            0x3 => Some(Instruction::SkipIfEqual(parts.x_register, parts.value)),
            0x4 => Some(Instruction::SkipIfNotEqual(parts.x_register, parts.value)),
            0x5 => Some(Instruction::SkipIfReqisterEqual(
                parts.x_register,
                parts.y_register,
            )),
            0x6 => Some(Instruction::StoreValue(parts.x_register, parts.value)),
            0x7 => Some(Instruction::AddValue(parts.x_register, parts.value)),
            0x8 => match parts.least_significant_bit {
                0x0 => Some(Instruction::CopyRegister(
                    parts.x_register,
                    parts.y_register,
                )),
                0x1 => Some(Instruction::OrRegister(parts.x_register, parts.y_register)),
                0x2 => Some(Instruction::AndRegister(parts.x_register, parts.y_register)),
                0x3 => Some(Instruction::XorRegister(parts.x_register, parts.y_register)),
                0x4 => Some(Instruction::AddRegister(parts.x_register, parts.y_register)),
                0x5 => Some(Instruction::SubRegister(parts.x_register, parts.y_register)),
                0x6 => Some(Instruction::ShiftRightRegister(parts.x_register)),
                0x7 => Some(Instruction::SwapSubRegister(
                    parts.x_register,
                    parts.y_register,
                )),
                0xE => Some(Instruction::ShiftLeftRegister(parts.x_register)),
                _ => None,
            },
            0x9 => Some(Instruction::SkipIfReqisterNotEqual(
                parts.x_register,
                parts.y_register,
            )),
            0xA => Some(Instruction::StoreIndex(parts.address)),
            0xB => Some(Instruction::JumpPlusZero(parts.address)),
            0xC => Some(Instruction::Random(parts.x_register, parts.value)),
            0xD => Some(Instruction::Draw(
                parts.x_register,
                parts.y_register,
                parts.least_significant_bit as Value,
            )),
            0xE => match parts.value {
                0x9E => Some(Instruction::SkipIfPressed(parts.x_register)),
                0xA1 => Some(Instruction::SkipIfNotPressed(parts.x_register)),
                _ => None,
            },
            0xF => match parts.value {
                0x07 => Some(Instruction::LoadDelayTimer(parts.x_register)),
                0x0A => Some(Instruction::WaitForKeyPress(parts.x_register)),
                0x15 => Some(Instruction::SetDelayTimer(parts.x_register)),
                0x18 => Some(Instruction::SetSoundTimer(parts.x_register)),
                0x1E => Some(Instruction::AddToIndex(parts.x_register)),
                0x29 => Some(Instruction::LoadSprite(parts.x_register)),
                0x33 => Some(Instruction::StoreBcdRepresenation(parts.x_register)),
                0x55 => Some(Instruction::StoreRegisters(parts.x_register)),
                0x65 => Some(Instruction::LoadRegisters(parts.x_register)),
                _ => None,
            },
            _ => None,
        }
    }
}
