mod chip8;
mod instruction;
mod io;
use std::{env, process, thread, time};

const TICK_RATE: u64 = 60;
const SKIP_TICKS: time::Duration = time::Duration::from_nanos(100000000 / TICK_RATE);

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() == 1 {
        println!("Run the programm with a path to a ROM");
        process::exit(1);
    }
    let filename = &args[1];

    // init io
    let mut io = io::IO::new();

    // init chip8
    let mut my_chip8 = chip8::Chip8::new();
    my_chip8.load_game(&filename);

    loop {
        let start = time::Instant::now();
        // emulation loop
        // - emulate one cycle
        my_chip8.emulate_cycle();
        // - draw screen
        io.update_display(my_chip8.display);
        // handle input
        my_chip8.keys = match io.check_keyboard(my_chip8.keys) {
            Some(keys) => keys,
            None => break,
        };
        // handle sound
        io.check_and_play_sound(my_chip8.sound_timer);
        // make it run at 60Hz
        let end = time::Instant::now().duration_since(start);
        let time_to_sleep = SKIP_TICKS
            .checked_sub(end)
            .unwrap_or(time::Duration::from_nanos(0));
        thread::sleep(time_to_sleep);
    }
    println!("Shutting down..");
}
