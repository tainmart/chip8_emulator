extern crate sdl2;

use crate::chip8::{DISPLAY_HEIGHT, DISPLAY_WIDTH, NUM_KEYS};

use sdl2::pixels::Color;
use sdl2::rect::Rect;
use sdl2::render::Canvas;
use sdl2::video::Window;

use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::EventPump;

use sdl2::audio::AudioSpecDesired;

const WINDOW_WIDTH: u32 = 640; //1280;
const WINDOW_HEIGTH: u32 = 320; //640;

pub struct IO {
    canvas: Canvas<Window>,
    event_pump: EventPump,
    audio: sdl2::audio::AudioQueue<i16>,
}

impl IO {
    pub fn new() -> Self {
        let sdl_context = sdl2::init().unwrap();
        let video_subsystem = sdl_context.video().unwrap();

        let window = video_subsystem
            .window("chip-8", WINDOW_WIDTH, WINDOW_HEIGTH)
            .position_centered()
            .build()
            .unwrap();

        let mut canvas = window.into_canvas().build().unwrap();
        canvas.set_draw_color(Color::RGB(0, 0, 0));
        canvas.clear();
        canvas.present();

        let event_pump = sdl_context.event_pump().unwrap();

        let audio_subsystem = sdl_context.audio().unwrap();
        let desired_spec = AudioSpecDesired {
            freq: Some(44100),
            channels: Some(1),
            samples: None,
        };

        let audio_device = audio_subsystem
            .open_queue::<i16, _>(None, &desired_spec)
            .unwrap();

        IO {
            canvas: canvas,
            event_pump: event_pump,
            audio: audio_device,
        }
    }

    pub fn check_keyboard(&mut self, mut keys: [bool; NUM_KEYS]) -> Option<[bool; NUM_KEYS]> {
        for event in self.event_pump.poll_iter() {
            match event {
                Event::Quit { .. }
                | Event::KeyDown {
                    keycode: Some(Keycode::Escape),
                    ..
                } => return None,
                Event::KeyDown {
                    keycode: Some(keycode),
                    ..
                } => match keycode {
                    Keycode::Num1 => keys[0x1] = true,
                    Keycode::Num2 => keys[0x2] = true,
                    Keycode::Num3 => keys[0x3] = true,
                    Keycode::Num4 => keys[0xC] = true,
                    Keycode::Q => keys[0x4] = true,
                    Keycode::W => keys[0x5] = true,
                    Keycode::E => keys[0x6] = true,
                    Keycode::R => keys[0xD] = true,
                    Keycode::A => keys[0x7] = true,
                    Keycode::S => keys[0x8] = true,
                    Keycode::D => keys[0x9] = true,
                    Keycode::F => keys[0xE] = true,
                    Keycode::Z => keys[0xA] = true,
                    Keycode::X => keys[0x0] = true,
                    Keycode::C => keys[0xB] = true,
                    Keycode::V => keys[0xF] = true,
                    _ => (),
                },
                Event::KeyUp {
                    keycode: Some(keycode),
                    ..
                } => match keycode {
                    Keycode::Num1 => keys[0x1] = false,
                    Keycode::Num2 => keys[0x2] = false,
                    Keycode::Num3 => keys[0x3] = false,
                    Keycode::Num4 => keys[0xC] = false,
                    Keycode::Q => keys[0x4] = false,
                    Keycode::W => keys[0x5] = false,
                    Keycode::E => keys[0x6] = false,
                    Keycode::R => keys[0xD] = false,
                    Keycode::A => keys[0x7] = false,
                    Keycode::S => keys[0x8] = false,
                    Keycode::D => keys[0x9] = false,
                    Keycode::F => keys[0xE] = false,
                    Keycode::Z => keys[0xA] = false,
                    Keycode::X => keys[0x0] = false,
                    Keycode::C => keys[0xB] = false,
                    Keycode::V => keys[0xF] = false,
                    _ => (),
                },
                _ => (),
            };
        }
        return Some(keys);
    }

    // Draw from pixel array
    pub fn update_display(&mut self, display_data: [bool; DISPLAY_HEIGHT * DISPLAY_WIDTH]) {
        self.canvas.clear();
        let (window_witdh, window_height) = self.canvas.output_size().unwrap();
        let point_width = window_witdh / DISPLAY_WIDTH as u32;
        let point_heigth = window_height / DISPLAY_HEIGHT as u32;

        let mut rectangles: Vec<Rect> = Vec::new();
        self.canvas.set_draw_color(Color::RGB(255, 255, 255));
        for (index, pixel) in display_data.iter().enumerate() {
            if !*pixel {
                continue;
            }
            let x = ((index % DISPLAY_WIDTH) * point_width as usize) as i32;
            let y = ((index / DISPLAY_WIDTH) * point_heigth as usize) as i32;
            rectangles.push(Rect::new(x, y, point_width, point_heigth));
        }
        self.canvas.fill_rects(rectangles.as_slice()).unwrap();
        self.canvas.present();
        self.canvas.set_draw_color(Color::RGB(0, 0, 0));
    }

    pub fn check_and_play_sound(&mut self, sound_data: u8) {
        if sound_data == 0 {
            return;
        }
        let tone_volume = 1_000i16;
        let period = 115;
        let sample_count = 500;
        let mut wave = Vec::new();

        for x in 0..sample_count {
            wave.push(if (x / period) % 2 == 0 {
                tone_volume
            } else {
                -tone_volume
            });
        }
        self.audio.queue(&wave);
        self.audio.resume();
    }
}
